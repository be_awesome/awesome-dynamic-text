<?php

/* DT - Dynamiczne Teksty 
 * wersja 3
 * Biblioteka dynamicznych tekstów wraz z obsługą wielu jężyków.
 * Autorzy: Tomasz Stryjewski, Maciej Mikitiuk, Przemysław Sobolewski
 * 
 * ZMIENNE W SESJI:
 * ['dt']['id'] - id ostatnio pobranego elementu, domyślnie 0
 * 
 * ZMIENNE W COOKIES:
 * ['dt']['ln'] - język, domyślnie 'pl'
 * ['dt']['auth'] - autoryzacja czy jest prawo do edycji
 * 
 * PLIKI
 * dt.php
 * dt.js
 */


// STAŁE
define('SALT', 'zabezpieczenie_autoryzacji');  // stała do zabezpieczania autoryzacji
define('DT_DBHOST', '');                       // adres hosta bazy MySQL
define('DT_DBUSER', '');                       // użytkownik w MySQL
define('DT_DBPASSWORD', '');                   // hasło w MySQL


// WYWOŁYWANIE FUNKCJI DO REQUEST LUB ZAREJSTROWANIE PLUGINÓW DO SMARTÓW
if ( isset( $_POST['dt_request']) )
{
    call_user_func($_POST['dt_request'], $_POST);
    
} else {
    // REJESTRACJA PLUGINÓW
    $smarty->registerPlugin("function", "dt_start", "dt_start");
    $smarty->registerPlugin("function", "dt", "dt");
    $smarty->registerPlugin("function", "dt_con", "dt_con");    
    $smarty->registerPlugin("function", "dt_sidebar", "dt_sidebar");    
    
} 

// INICJACJA DT
function dt_start() 
{
    $_SESSION['dt']['id'] = 1;
    echo '<script src="'.baseUrl().'/scripts/dt.js"></script>';

}


/**
 * DT 
 *
 * @param int $id - id tekstu do pobrania, domyślnie pobierane z $_SESSION['dt']['id']
 * @param string $page - strona z której pobierany jest tekst, domyślnie z $_SERVER['PATH_INFO']
 * @param string def - domyślny tekst dodany do bazy jeżeli brak, domyślnie '! BRAK TEKSTU!'
 * @param array var - zmienne dodawane do tekstu, 'Witaj %%1, twój mail to %%2'
 * @param string const - pobiera tekst o zadanym id z tabeli stałych tekstów (np.: 'TAK', 'WYŚLIJ', 'DRUKUJ')
 * @return string - wartosc dynamicznego tekstu 
 */
function dt($params) 
{
    $ln = dt_getln();

    // POBIERANIE STRONY i ID
    if (isset($params['page'])) 
    {
        $page = $params['page'];
        $id = $params['id'];
        
    } else {
        $page = dt_clearURI();
        if (isset($params['id'])) 
        {
            $id = $params['id'];
            
        } else {
            $id = $_SESSION['dt']['id'];
            $_SESSION['dt']['id'] ++;
            
        }
        
    }

    // POBIERANIE TRESCI
    $text = dt_getbyid($id, $page, $ln);


    // DODWANIE W PRZYPADKU BRAKU W BAZIE
    if (!$text) 
    {
        $text = dt_insert($id, $page, $ln, $params['def']);
        
    }
    
    // DODAWANIE ZMIENNYCH
    if ( isset($params['var']) ) 
    {
        $var_text = $text;
        
        $var = explode("||", $params['var']);
        
        foreach ($var as $k => $w) 
        {
            $co = '%%' . ($k + 1);
            $text = str_replace($co, $w, $text);
            
        }
        
    }
           
    // DODAWANIE EDYCJI DLA TLUMACZA    
    if ( isset($_COOKIE['dt']['auth']) )
    {
        if($_COOKIE['dt']['auth'] == md5(SALT)) 
        {
            $text = dt_addAdmin($text, $id, $page, $ln, $var_text ? $var_text : null );
            
        }
        
    }
    
    return $text;
    
}

// DO NAPISANIA PRZY ROBIENU BIBLIOTEKI, W SMARTACH NIE POTRZEBNA
function dt_def($params) 
{
    
}

function dt_con($params) 
{
    $ln = dt_getln();
    $id = $params['con'];

    // POBIERANIE TRESCI
    $text = dt_getbycon($id, $page, $ln);

    // DODWANIE W PRZYPADKU BRAKU W BAZIE
    if (!$text) 
    {
        $text = dt_insert($id, 'const', $ln, $id);
        
    }
    
    // DODAWANIE EDYCJI DLA TLUMACZA    
    if ( isset($_COOKIE['dt']['auth']) )
    {
        if($_COOKIE['dt']['auth'] == md5(SALT)) 
        {
            $text = dt_addAdmin($text, $id, $page, $ln, $var_text ? $var_text : null );
            
        }
        
    }

    return $text;
    
}

// FUNKCJE


/** DODAWANIE FUNKCJI TŁUMACZENIA
 * 
 * @param string $text - wyświetlany takst
 * @param string $id - id tekstu
 * @param string $page - strona tekstu
 * @param string $ln - język tekstu
 * @param string $var_text - tekst bez zamienionych zmiennych (%%1)
 * @return string
 */
function dt_addAdmin($text, $id, $page, $ln, $var_text )
{
    $dt = 'dt_' . $page . '_' . $id;
    $var = $var_text ? "data-dt_var='$var_text'" : '';
    return "<font class='dt_text' id='$dt' data-dt_id='$id' data-dt_page='$page' data-dt_ln='$ln' $var>" . $text . "</font> ";

}


/**
 * DODWANIE TRESCI
 * 
 * @param int $id - id tekstu 
 * @param string $page - strona 
 * @param string $ln - język tekstu 
 * @param string $default - tekst która ma zostać dodany, jeżeli pusty doda się '! BRAK TEKSTU'
 * @return string - tekst do wyswietlenia
 * 
 */
function dt_insert($id, $page, $ln, $default) 
{
    $default = is_null($default) ? '! BRAK TEKSTU !' : $default;
    $sql = "INSERT INTO dt_text (id, page, text, ln) VALUES ('$id', '$page', '$default', '$ln')";
    $li = mysql_query($sql);

    return $default;
    
}

/**
 * POBIERANIE TRESCI
 * 
 * @param int $id - id pobieranego tekstu
 * @param string $page - strona pobieranego tekstu
 * @param string $ln - język pobieranego tekstu
 * @return string - dynamiczny tekst do wyswietlenia
 * 
 */
function dt_getbyid($id, $page, $ln) 
{
    $sql = "SELECT * FROM dt_text WHERE page = '$page' AND id='$id' AND ln='$ln'";
    $li  = mysql_query($sql);
    $ile = mysql_num_rows($li);

    if ($ile == 0) 
    {
        return null;
        
    } else {
        $row = mysql_fetch_assoc($li);
        return $row['text'];
        
    }
    
}

function dt_getbycon($id, $page, $ln)
{
    $sql = "SELECT * FROM dt_text WHERE page = 'const' AND id='$id' AND ln='$ln'";
    $li  = mysql_query($sql);
    $ile = mysql_num_rows($li);

    if ($ile == 0) 
    {
        return null;
        
    } else {
        $row = mysql_fetch_assoc($li);
        return $row['text'];
        
    }
    
}

/**
 * POBIERANIE JĘZYKA
 * 
 * @return string - skrót języka, domyślnie 'pl'
 */
function dt_getln() 
{
    // USTQWIANIE JĘZYKA
    if (isset($_COOKIE['ln'])) 
    {
        $ln = $_COOKIE['ln'];
        
    } else {
        $ln = 'pl';
        
    }

    return $ln;
    
}

/**
 * CZYSZCZENIE ADRESU STRONY
 * 
 * @return string - adres bez '/' na początku oraz rozszerzenia i zmiennych GET
 */
function dt_clearURI()
{
    $address = ( $_SERVER['SCRIPT_NAME'][0] == '/' ) ? substr($_SERVER['SCRIPT_NAME'],1) : $_SERVER['SCRIPT_NAME'];
    return explode('.', $address)[0];
    
}
 

/**
 * REQUEST DO UPDATU TABELI dt_text PO WPROWADZENIU ZMIAN. 
 * @param array $params - POST ze zmiennymi z requesta
 */
function dt_update_request($params)
{
    if ( isset($_COOKIE['dt']['auth']) )
    {
        foreach ($_POST as $key => $value) $$key = $value; 
        dt_connect();
        $sql = "UPDATE dt_text SET text='$text' WHERE id='$id' AND page='$page' AND ln='$ln'";
        mysql_query($sql);  
        $response['sql'] = $sql;
        $response['status'] = 'Pomyslna autoryzacja';
        
    } else {
        $response['status'] = 'Brak autoryzacji';
        
    }  
        
    echo json_encode($response);  
    
}



/**
 * POLACZENIE Z BAZĄ DANYCH
 * @return int|string
 * 
 */
function dt_connect()
{
    if ($li = mysql_connect(DT_DBHOST, DT_DBUSER, DT_DBPASSWORD)) 
    {
        mysql_select_db(DT_DBUSER, $li);
        mysql_query("SET NAMES utf8");
        return 1;
        
    } else {
        return "Brak połączenia z bazą danych";
        
    }
    
    if(mysql_num_rows(mysql_query("SHOW TABLES LIKE 'dt_text'"))==0)
    {
        "DROP TABLE IF EXISTS `flopik_22`.`dt_text`;
            CREATE TABLE  `flopik_22`.`dt_text` (
              `id_dt` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `page` text NOT NULL,
              `id` text NOT NULL,
              `text` text NOT NULL,
              `ln` varchar(2) NOT NULL,
              PRIMARY KEY (`id_dt`)
            ) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin2;";

    }
    
}

function dt_sidebar()
{
    if (isset($_COOKIE['dt']['auth']))
    {
?>

<style>
    #dt_sidebar{
        left: -280px;     
        background: #2c3e50;
        width: 305px;
        min-height: 100%;
        position: fixed;
        left: -295px;
        top: 0;
        padding: 25px 25px 0 25px;
        -o-transition: all 160ms;
        -moz-transition: all 160ms;
        -webkit-transition: all 160ms;
        transition: all 160ms;
        border-right: 1px solid #E1E2E1;
        z-index: 99999;
        color: #f1c40f;
    }

    #dt_sidebar .btnPT {
        position: absolute;
        left: 290px;
        font-size: 28px;    
        color: white;
    }

    #dt_sidebar .slide{
        top: 31px;
        cursor: pointer;
    }

    #dt_sidebar .eye{
        top: 95px;    
        cursor: pointer;
    }


    #dt_sidebar .deg180 {
        -ms-transform: rotate(180deg); /* IE 9 */
        -webkit-transform: rotate(180deg); /* Chrome, Safari, Opera */
        transform: rotate(180deg);     
        -o-transition: all 500ms;
        -moz-transition: all 500ms;
        -webkit-transition: all 500ms;
        transition: all 500ms;
    }

    #dt_sidebar .deg0 {
        -ms-transform: rotate(0deg); /* IE 9 */
        -webkit-transform: rotate(0deg); /* Chrome, Safari, Opera */
        transform: rotate(0deg);     
        -o-transition: all 500ms;
        -moz-transition: all 500ms;
        -webkit-transition: all 500ms;
        transition: all 500ms;
    }

    #dt_sidebar h2 {                
        margin: 0px;
        padding: 0px;
        font-size: 25px;
        font-weight: bold;
    }

    #dt_sidebar input
    {
        width: 100%;
        padding: 5px 7px;

    }

    #dt_sidebar .blink {
        -webkit-animation-direction: normal;
        -webkit-animation-duration: 2s;
        -webkit-animation-iteration-count: infinite;
        -webkit-animation-name: blink;
        -webkit-animation-timing-function: ease;   
    }

    @-webkit-keyframes 'blink' {
        0% { background: rgba(255, 216, 0, 0.5) }
        50% { background: rgba(255, 216, 0, 0.2) }
        100% { background: rgba(255, 216, 0, 0.5) }
    }

    #dt_sidebar .deg45 {
        -ms-transform: rotate(45deg); /* IE 9 */
        -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
        transform: rotate(45deg);     
    }

    #dt_point {
        position: absolute;
        left: 600px;
        top: 500px;
        color: red;
        font-size: 15px;
        margin-left: -40px;
        margin-top: -25px;
        display: none;
    }
    
    .dt_class_body {
        -o-transition: all 160ms;
        -moz-transition: all 160ms;
        -webkit-transition: all 160ms;
        transition: all 160ms;
        position: absolute;
        min-width: 1200px;
    }
</style>

    <div id="dt_sidebar">
        <span class="btnPT slide fa-stack fa-lg">
            <i class="fa fa-square fa-stack-2x" style="color: #2c3e50"></i>
            <i id="dt_sidebar-arrow" class="fa fa-chevron-right fa-stack-1x fa-inverse" style="color: #f1c40f"></i> 
        </span>

        <span class="btnPT eye fa-stack fa-lg">
            <i class="fa fa-square fa-stack-2x" style="color: #2c3e50"></i>
            <i id="dt_sidebar-eye" class="fa fa-eye fa-stack-1x fa-inverse" style="color: #f1c40f"></i> 
        </span>

        <div id="dt_point" class="">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x" style="color: red" style="color: #2c3e50"></i>
                <i class="fa fa-hand-o-right fa-stack-1x fa-inverse deg45 " style="color: #f1c40f"></i> 
            </span>
        </div>

        <h2> PANEL TŁUMACZEŃ </h2>
        <hr>
        <div id="lista" >

        </div>

    </div>

<script>
    $(document).ready(function () {
        body_width = $('body').css("width");
        $('body').addClass('dt_class_body');
        console.log (body_width);
        $('body').css("width", body_width)
    });
</script>

        <?php
    }
    
}


// BEZPOŚREDNI ADRES STRONY - jest już dtmt
////function baseUrl() {
//    return $link = "http://" . $_SERVER['SERVER_NAME'] . "";
//}