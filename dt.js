// ADRES DO PLIKU dt.php
var DT_ADDRESS = "/dt.php";

$(document).ready(function () {
    // DODANIE MODALA DO TLUMACZEN DO STRONY
    var dt_modal = '<div class=\'modal fade\' id=\'dt_modal\' tabindex=\'-1\' role=\'dialog\' aria-labelledby=\'Modal tłumaczeń\'>\n<div class=\'modal-dialog\' role=\'document\'>\n<div class=\'modal-content\'>\n<div class=\'modal-body\'>\n<button type=\'button\' class=\'close\' data-dismiss=\'modal\' aria-label=\'Close\'> <i style="color: #000" class="fa fa-times-circle"></i> </button>\n<br><br></div>\n<div class=\'modal-footer\'>\n<button type=\'button\' class=\'btn btn-default\' data-dismiss=\'modal\'>Anuluj</button>\n<button type=\'button\' id=\'dt_save\' class=\'btn btn-primary\'>Zapisz</button>\n</div>\n</div>\n</div>\n</div>';
    $('body').append(dt_modal);
    
    // WYSWIETLENIR MODALA DO TLUMACZEN
    $('.dt_text').on("contextmenu", function(){
        $('#dt_modal').modal('show');
        $('#dt_modal input, #dt_modal textarea').remove();
        
        // PPOBRANIE TEKSTU Z WYBRANEGO DT
        text = $(this).attr('data-dt_var') ? $(this).attr('data-dt_var') : $(this).html();
        
        // POBRANIE DATA ATRYBUTÓW Z WYBRANEGO DT i DODANIE DO MODALA
        dt_data_attr = '';
        $.each($(this).data(), function(key, value){
            $('#dt_save').attr('data-'+key, value)
        });
        
        // WYSWIETLENIE INPUTA DLA TRESCI MNIEJSZEJ NIZ 65 i TEXTAREA DLA WIEKSZEJ
        if ( text.length < 65 ) 
        {
            $('#dt_modal .modal-body').append("<input id='dt-newtext' type='text' class='form-control'>");
            $('#dt_modal input').val(text);
            
        } else {
            $('#dt_modal .modal-body').append("<textarea id='dt-newtext' rows='14' class='form-control' ></textarea>").val(text);
            $('#dt_modal textarea').val(text);
            
        }
        
        // AUTOMATYCZNE PRZELACZANIE INPUT NA TEXTAREA W ZALEZNOSCI OD DLUGOSCI TEKSTU
        //$('#dt_modal input, #dt_modal textarea').on("keyup", function(){
        $('#dt_modal input, #dt_modal textarea').keypress(function(event) {
            if (event.which == 13) { $('#dt_save').trigger( "click" ); }
            dt_input_toggle(this);
 
        });
        
        // ZAPISYWANIE TRESCI
        $('#dt_save').on("click", function(){
            text = $('#dt-newtext').val();
            id = $('#dt_save').data('dt_id');
            ln = $('#dt_save').data('dt_ln');
            page = $('#dt_save').data('dt_page');
            $.ajax({
                url: DT_ADDRESS,
                method: "POST",
                dataType: 'json',
                data: {
                        dt_request: 'dt_update_request',
                        text: $('#dt-newtext').val(),
                        id: $('#dt_save').data('dt_id'),
                        ln: $('#dt_save').data('dt_ln'),
                        page: $('#dt_save').data('dt_page'),
                    },
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data.responseText);
                }
            });
            
            $('#dt_'+page+'_'+id).html(text);
            if( $('#dt_'+page+'_'+id).attr('data-dt_var') ) 
            { 
                $('#dt_'+page+'_'+id).attr("data-dt_var", text);
            }
            
            $('#dt_modal').modal('hide');
            
        });
        
        return false;   
    })
    
    // AUTOMATYCZNE PRZELACZANIE INPUT NA TEXTAREA W ZALEZNOSCI OD DLUGOSCI TEKSTU
    function dt_input_toggle(ele){
        text = $(ele).val();
        if ( text.length > 65 && $('#dt_modal textarea').length == 0 ) 
        {
            $('#dt_modal input').remove();
            $('#dt_modal .modal-body').append("<textarea id='dt-newtext' rows='14' class='form-control'></textarea>").val(text);
            $('#dt_modal textarea').val(text);
            $('#dt_modal textarea').focus();
            $('#dt_modal input, #dt_modal textarea').on("keyup", function(){
                dt_input_toggle(this);
            });
            
        } 
        
        if ( text.length < 65 && $('#dt_modal input').length == 0 ) {
            $('#dt_modal textarea').remove();
            $('#dt_modal .modal-body').append("<input id='dt-newtext' type='text' class='form-control'>");
            $('#dt_modal input').val(text);
            $('#dt_modal input').focus();
            $('#dt_modal input, #dt_modal textarea').on("keyup",  function(){
                dt_input_toggle(this);
            });

        }        
    }
    
    
    
});

/// DT SIDEBAR

$(document).ready(function () {
    // OBSLUGA WYSUWANIA i WSUWANIA SIDEBARA
    $("#dt_sidebar .slide").on("click", function () {
        if ($("#dt_sidebar").css("left") == "0px")
        {
            $("#dt_sidebar").css("left", "-280px");
            $("#dt_sidebar-arrow").removeClass("deg180");
            $("#dt_sidebar-arrow").addClass("deg0");

        } else {
            $("#dt_sidebar").css("left", "0px");
            $("#dt_sidebar-arrow").removeClass("deg0");
            $("#dt_sidebar-arrow").addClass("deg180");
        }
    });
    //$(".slide").trigger("click");

    // PODSWIETL WSZYTSKIE DYNAMICZNE TEKSTY
    $(".eye").on("click", function () {
        ele = $(this).children()[1];
        if ($(ele).attr('class').indexOf("fa-eye-slash") > 0) {
            $('[data-dtmt]').removeClass('blink');
            $(ele).addClass("fa-eye");
            $(ele).removeClass("fa-eye-slash");
        } else {
            $('[data-dtmt]').addClass('blink');
            $(ele).removeClass("fa-eye");
            $(ele).addClass("fa-eye-slash");
        }
    });

    // DODAWANIE DYNAMICZNYCH TEKSTÓW DO PANELU PO TLUMACZEN
    ele = $('[data-dtmt]');
    var dtmt = new Array;
    $.each(ele, function (key, value) {
        tresc = $(value).html();
        tresc = $(value).html() ? $(value).html() : $(value).data("tresc");
        strona = $(value).data("strona");
        ln = $(value).data("ln");
        typ = $(value).data("typ");
        id = $(value).data('dtmt');



        app = "<input data-plid='" + id + "' data-typ='" + typ + "' data-strona='" + strona + "' data-ln='" + ln + "' type='text' value='" + tresc + "'><small>" + tresc + " / " + strona + "</small><br/><br/>";
        $("#lista").append(app);
    });

    // ZAPISYWANIE Z POZIOMU PANELU TLUMACZEN
    $('[data-plid]').on("keyup", function () {
        id = $(this).data('plid');
        tresc = $(this).val();
        strona = $(this).data('strona');
        typ = $(this).data('typ');
        $.ajax({
            url: "<? echo $link ?>//php/request/r_zapiszText.php",
            type: 'GET',
            data: {r: id,
                t: tresc,
                q: strona,
                s: $(this).data('typ'),
                u: $(this).data('ln'),
            },
            success: function (data) {
                iddtmt = '#dtmt_' + strona + '_' + id;
                console.log(typ);
                if (typ == 3)
                {
                    $('[data-dtmt="' + id + '"]').attr("placeholder", tresc);

                } else {
                    $(iddtmt).html(tresc);
                }


            }
        });
    });

    //PODSWIETLANIE ZAZNACZONEGO POLA
    $("#lista input").on("click", function () {
        id = $(this).data('plid');
        strona = $(this).data('strona');
        dtmtid = "dtmt_" + strona + '_' + id;
        console.log(dtmtid);
        var off = $("#" + dtmtid).offset();
        if (off.left > 0) {
            $("#dt_point").css("display", "block");
            $("#dt_point").css("left", off.left + "px");
            $("#dt_point").css("top", off.top + "px");
        }
    });

    $("#lista input").focusout(function () {
        $("#dt_point").css("display", "none");
    });


});